<?php

/**
 * @file
 * BotUnfuddle class.
 */

/**
 * We need a way to get severities.
 */
class BotUnfuddle extends Unfuddle {
  public function getSeverities($projectID) {
    $projectID = is_null($projectID) ? $this->project : $projectID;
    try {
      $ticket = $this->request($this->url . '/api/v1/projects/' . $projectID . '/severities');
      return $ticket;
    }
    catch (Exception $e) {
      $message = $e->getMessage();
      watchdog('unfuddle_api', 'Unable to retrieve Unfuddle severities for project !id. Error message @message', array('!id' => $projectID, '@message' => $message), WATCHDOG_ERROR);
      return FALSE;
    }
  }
}
